(ns arangodb-java-wrapper.core
  (:import (java.util Properties)
           (com.arangodb ArangoCollection ArangoCursor ArangoDB ArangoDBException)
           (com.arangodb.entity BaseDocument CollectionEntity)
           (com.arangodb.util MapBuilder)
           (com.arangodb.velocypack VPackSlice)
           (com.arangodb.velocypack.exception VPackException))
  (:gen-class))

(defn make-arangodb
  "Build the arangodb configuration with the arangodb builder,
  return an ArangoDB object.
  NOTE: If you set useSsl to true,you must provide a SSLContext object
  at the same time."
  ([]
   (-> (com.arangodb.ArangoDB$Builder.)
       (.host "localhost")
       (.port (int 8529))
       (.timeout (int 0))
       (.user "")
       (.password "")
       (.useSsl false)
       (.chunksize (int 30000))
       (.build)))
  ([{:keys [host port timeout user password useSsl chunksize]} & [sc]]
   (if (and useSsl (nil? sc))
     (throw (ex-info "Must provide a SSLContext object if useSsl is true." {:useSsl useSsl :sslcontext sc})))
   (-> (com.arangodb.ArangoDB$Builder.)
       (.host (or host "localhost"))
       (.port (int (or port 8529)))
       (.timeout (int (or timeout 0)))
       (.user (or user ""))
       (.password (or password ""))
       (.useSsl (or useSsl false))
       (.sslContext sc)
       (.chunksize (int (or chunksize 30000)))
       (.build))))

(defn shutdown [aDB]
  (.shutdown aDB))

(defn query
  "Query by AQL. It can accept an optional bindings and options map.
  Currently,it returns a lazy sequence with JSON."
  [arangodb db-name query & [binds options]]
  (-> arangodb
      (.db db-name)
      (.query query binds options String)
      (iterator-seq)))
