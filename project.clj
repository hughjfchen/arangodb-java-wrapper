(defproject arangodb-java-wrapper "0.1.0"
  :description "A clojure wrapper for arangodb 3.x java driver"
  :url "http://github.com/hughjfchen/arangodb-java-wrapper"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.arangodb/arangodb-java-driver "4.1.3"]])
